﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class CheckController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public CheckController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo
        [HttpGet]
        public async Task Get()
        {
            var cmd = new SqlCommand("Exec SP_get_CheckPoints 0 ");
            await SqlPipe.Stream(cmd, Response.Body, "{}");


        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{idx}")]
        public async Task Get(string Username, string Password, string dt, string keys, double idx)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_get_CheckPoints @id ");
                cmd.Parameters.AddWithValue("idx", idx);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        // POST api/Todo
        [HttpPost()]
        public async Task Post()
        {

            //if (key == CintaIDAPI.CLS_FUNCT.Base64Encode(key.ToString()))
            //{

                string Tr_checkPointsJson = new StreamReader(Request.Body).ReadToEnd();
               // INS_Intruders(LoginJson);
                var cmd = new SqlCommand("Exec Ins_Tr_checkPoints_Json @Tr_checkPointsJson");
                cmd.Parameters.AddWithValue("@Tr_checkPointsJson", Tr_checkPointsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
               
            //}
            //else
            //{
            // INS_Intruders(key);

            //}


        }


    }
}
