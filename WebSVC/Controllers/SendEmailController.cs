﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;
using System;
using MimeKit;
using MailKit.Net.Smtp;
using System.Net;
using MailKit.Security;
using MailKit.Net.Imap;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class SendEmailController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public SendEmailController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        //public async Task Get(string Username, string Password, string dt, string keys,string paramJson)
        //{
        //    string deCode = "";
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        deCode = CintaIDAPI.CLS_FUNCT.Base64Decode(paramJson);
        //        var cmd = new SqlCommand("Exec SP_Get_Withdrawal @paramJson ");
        //        cmd.Parameters.AddWithValue("paramJson", deCode);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    }
        //    else
        //    {
        //        await INS_Intruders(Username);

        //    }
        //}

        // POST api/Todo
        [HttpGet("{email}/{subject}/{message}")]
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress("Rentals Rated", "goldeneye.cintaid@gmail.com"));

                emailMessage.To.Add(new MailboxAddress("Imran Ahmad", email));
                emailMessage.Subject = subject;
                emailMessage.Body = new TextPart("plain") { Text = message };

                using (var client = new SmtpClient())
                {
                    var credentials = new NetworkCredential
                    {
                        UserName = "goldeneye.cintaid@gmail.com",
                        Password = "cinta2018"
                    };
                    //      client.LocalDomain = "gmail.com";
                    await client.ConnectAsync("smtp.gmail.com", 465, SecureSocketOptions.Auto).ConfigureAwait(false);
                    await client.AuthenticateAsync(credentials);
                    await client.SendAsync(emailMessage).ConfigureAwait(false);
                    await client.DisconnectAsync(true).ConfigureAwait(false);
                }

                //=============================================


                //var emailMessage = new MimeMessage();

                //emailMessage.From.Add(new MailboxAddress("Joe Bloggs", "goldeneye.cintaid@gmail.com"));
                //emailMessage.To.Add(new MailboxAddress("imran ahmad", email));
                //emailMessage.Subject = subject;
                //emailMessage.Body = new TextPart("Test plain") { Text = message };

                //using (var client = new SmtpClient())
                //{
                //    client.LocalDomain = "smtp.gmail.com";
                //    await client.ConnectAsync("smtp.gmail.com", 587);
                //    await client.SendAsync(emailMessage).ConfigureAwait(false);
                //    await client.DisconnectAsync(true).ConfigureAwait(false);
                //}
            }
            catch(Exception ex)
            {
                string msg = ex.ToString();
            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        
        


    }
}
