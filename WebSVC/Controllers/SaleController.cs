﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class SaleController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public SaleController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            string deCode = "";
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                deCode = CintaIDAPI.CLS_FUNCT.Base64Decode(paramJson);

                var cmd = new SqlCommand("exec SP_GET_TR_SALES @paramJson");
                cmd.Parameters.AddWithValue("paramJson", deCode);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }


        //// GET api/Todo
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double id)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("exec SP_Get_Sales_ID @id");
        //        cmd.Parameters.AddWithValue("id", id);
        //        await SqlPipe.Stream(cmd, Response.Body, "[]");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }
        //}

        ////@Merchant_ID bigint, @ID as bigint, @App as small_String,@type as bit
        //// GET api/Todo
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{App}/{Status}/{Search}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double id, string App, bool Status,string Search)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        //@Username as bigint, @App as small_String,@type as bit,@search as middle_string
        //        var cmd = new SqlCommand("exec SP_Sales_Trans @Username, @App, @Type, @Search");
        //        cmd.Parameters.AddWithValue("Username", id);
        //        cmd.Parameters.AddWithValue("App", App);
        //        cmd.Parameters.AddWithValue("Type", Status);
        //        cmd.Parameters.AddWithValue("Search", Search);
        //        await SqlPipe.Stream(cmd, Response.Body, "[]");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }
        //}



        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_id}/{search}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double Merchant_id, string Search)
        //{

        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("EXEC SP_Get_Sales_Search @Merchant_ID, @search");
        //        cmd.Parameters.AddWithValue("Merchant_id", Merchant_id);
        //        cmd.Parameters.AddWithValue("search", Search);
        //        await SqlPipe.Stream(cmd, Response.Body, "[]");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);
        //    }

        //}

        // POST api/Todo
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string TR_SalesJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_Sales_Json @TR_SalesJson");
                cmd.Parameters.AddWithValue("TR_SalesJson", TR_SalesJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }



        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_SalesJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Sales_id @Tr_SalesJson");
                cmd.Parameters.AddWithValue("Tr_SalesJson", Tr_SalesJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
