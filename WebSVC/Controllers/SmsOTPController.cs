﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;
using System;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class SmsOTPController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public SmsOTPController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        
        static HttpClient client = new HttpClient();
        
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            try
            {
                if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
                {
                    var httpClient = new HttpClient();
                    var request_json = new StreamReader(Request.Body).ReadToEnd();

                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Y2ludGEuaWQ6MjVmZjkxMGM5MGJkYmE4ZjNlZmI5NmZkNTI1ZTc4Mzg=");
                    httpClient.DefaultRequestHeaders.Add("HTTPMethod", "POST");
                    var content = new StringContent(request_json, Encoding.UTF8, "application/json");
                    var post = httpClient.PostAsync("https://gateway.citcall.com/v3/smsotp", content);
                    post.Wait();
                    var contents = post.Result.Content.ReadAsStringAsync().Result;

                    await post.Result.Content.ReadAsStringAsync();

                    //HttpClient client = new HttpClient();
                    ////string myJson = new StreamReader(Request.Body).ReadToEnd();
                    //var request_json = new StreamReader(Request.Body).ReadToEnd();

                    //client.DefaultRequestHeaders.Accept.Clear();
                    //var content = new StringContent(request_json, Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Y2ludGEuaWQ6MjVmZjkxMGM5MGJkYmE4ZjNlZmI5NmZkNTI1ZTc4Mzg=");  //Production

                    //var result = await client.PostAsync("https://gateway.citcall.com/v3/smsotp", content);

                    //await result.Content.ReadAsStringAsync();


                    //string responsXendit = "";
                    //HttpClient client = new HttpClient();
                    ////string myJson = new StreamReader(Request.Body).ReadToEnd();
                    //var request_json = new StreamReader(Request.Body).ReadToEnd();

                    //var content = new StringContent(request_json, Encoding.UTF8, "application /json");

                    ////client.DefaultRequestHeaders.Add("Authorization", "basic eG5kX2RldmVsb3BtZW50X09vNkVmTDhrZ09hdXg1VTRmZUJKRWo2VVpJZjM5Tk44eENlelJ4bldIUWJ5b0N3OTBoQTo=");  //Development
                    //client.DefaultRequestHeaders.Add("Authorization", "Y2ludGEuaWQ6MjVmZjkxMGM5MGJkYmE4ZjNlZmI5NmZkNTI1ZTc4Mzg=");  //Production

                    //var result = await client.PostAsync("https://gateway.citcall.com/v3/smsotp", content);
                    //string hasil = result.ToString();
                    //await result.Content.ReadAsStringAsync();


                }
                else
                {
                    await INS_Intruders(Username);

                }

            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        
        


    }
}
