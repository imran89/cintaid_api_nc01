﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MostFavoriteController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MostFavoriteController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }
        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{TypeID}/{App}")]
        public async Task Get(string Username, string Password, string dt, string Id, string keys, int TypeID, string App)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Most_Favorite_Json @TypeID, @App ");
                cmd.Parameters.AddWithValue("TypeID", TypeID);
                cmd.Parameters.AddWithValue("App", App);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);

            }          
          }

            public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
        

    }
}
