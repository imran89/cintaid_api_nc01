﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class FollowController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public FollowController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Customer_ID}/{Follow_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Customer_ID, string Follow_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_FollowList @Customer_ID,@Follow_ID ");
                cmd.Parameters.AddWithValue("Customer_ID", Customer_ID);
                cmd.Parameters.AddWithValue("Follow_ID", Follow_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Customer_ID}/{Search}/{Type}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Customer_ID, string Search, string Type)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_MyFollowList @Customer_ID, @Search, @Type ");
                cmd.Parameters.AddWithValue("Customer_ID", Customer_ID);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Type", Type);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);

            }          
          }

            public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }

        

    // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            string Tr_FriendsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Ins_Tr_Follows_Json @Tr_FollowsJson");
            cmd.Parameters.AddWithValue("Tr_FollowsJson", Tr_FriendsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Delete(string Username, string Password, string dt, string keys, double id)
        {
            var cmd = new SqlCommand(@"EXEC Del_TR_Follows @id");
            cmd.Parameters.AddWithValue("@id", id);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


    }
}
