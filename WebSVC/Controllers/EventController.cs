﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public EventController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Id}")]
        public async Task Get(string Username, string Password, string dt, string keys,string Id,double Customer_Id)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Event_ID @id ");
                cmd.Parameters.AddWithValue("id", Id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Id}/{Search}/{Long}/{Lad}/{Radius}")]
        public async Task Get(string Username, string Password, string dt, string keys, int Id, string Search, double Long, double Lad, double Radius)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Event_Search @Id, @Search, @Long, @Lad, @Radius");
                cmd.Parameters.AddWithValue("Id", Id);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Long", Long);
                cmd.Parameters.AddWithValue("Lad", Lad);
                cmd.Parameters.AddWithValue("Radius", Radius);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);

            }          
          }

            public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }

        

    // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            string Tr_EventsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Ins_Tr_Events_Json @Tr_EventsJson");
            cmd.Parameters.AddWithValue("Tr_EventsJson", Tr_EventsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{Detail}")]
        public async Task Post2(string Username, string Password, string dt, string keys, string Detail)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_EventsDetailJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Ins_Tr_EventsDetail_Json @Tr_EventsDetailJson");
                cmd.Parameters.AddWithValue("Tr_EventsDetailJson", Tr_EventsDetailJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // POST api/Todo for Friend Join Event
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{Detail}/{Join}/")]
        public async Task Post3(string Username, string Password, string dt, string keys, string Detail, string Join)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string EventJoinJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Ins_EventJoin_Json @EventJoinJson");
                cmd.Parameters.AddWithValue("EventJoinJson", EventJoinJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {
            string Tr_EventsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Del_Tr_Events @Tr_EventsJson");
            cmd.Parameters.AddWithValue("@Tr_EventsJson", Tr_EventsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


    }
}
