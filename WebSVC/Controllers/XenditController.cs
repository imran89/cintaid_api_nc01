﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;
using System;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class XenditController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public XenditController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task Get(string Username, string Password, string dt, string keys,string paramJson)
        {
            string deCode = "";
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                deCode = CintaIDAPI.CLS_FUNCT.Base64Decode(paramJson);
                var cmd = new SqlCommand("Exec SP_Get_Tr_Xendit @paramJson ");
                cmd.Parameters.AddWithValue("paramJson", deCode);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        static HttpClient client = new HttpClient();
        
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string responsXendit = "";
                using (HttpClient client = new HttpClient())
                {
                    //string myJson = new StreamReader(Request.Body).ReadToEnd();
                    var request_json = new StreamReader(Request.Body).ReadToEnd();

                    var content = new StringContent(request_json, Encoding.UTF8, "application/json");

                    //client.DefaultRequestHeaders.Add("Authorization", "basic eG5kX2RldmVsb3BtZW50X09vNkVmTDhrZ09hdXg1VTRmZUJKRWo2VVpJZjM5Tk44eENlelJ4bldIUWJ5b0N3OTBoQTo=");  //Development
                    client.DefaultRequestHeaders.Add("Authorization", "basic eG5kX3Byb2R1Y3Rpb25fUFlxRmZMOGtnT2F1eDVVNGZlQkpFajZVWklmMzlOTjh4Q2V6UnhuV0hRYnlvQ3c5MGhBOg==");  //Production

                    var result = await client.PostAsync("https://api.xendit.co/credit_card_charges", content);

                    var result_string = await result.Content.ReadAsStringAsync();
                    responsXendit = result_string.ToString();
                    
                }
                
                //string Tr_XenditJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Ins_Tr_Xendit_Json @Tr_XenditJson");
                cmd.Parameters.AddWithValue("Tr_XenditJson", responsXendit);
                await SqlPipe.Stream(cmd, Response.Body, "{}");


            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        
        


    }
}
