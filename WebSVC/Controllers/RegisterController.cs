﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public RegisterController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

       
        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{username}/{password}/{key}")]
        public async Task Get(string username,string password,string key)
        {
             if (key == CintaIDAPI.CLS_FUNCT.Base64Encode(username.ToString()))
             {
            

            var cmd = new SqlCommand("Exec SP_Register @username,@password");
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                await INS_Intruders(username);

            }
        }
        

        // POST api/Todo
        [HttpPost("{Username}/{dt}/{keys}")]
        public async Task Post(string Username, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string M_CustomersJson = new StreamReader(Request.Body).ReadToEnd();
                // INS_Intruders(LoginJson);
                var cmd = new SqlCommand("Exec Ins_M_CustomersFromJson @M_CustomersJson");
                cmd.Parameters.AddWithValue("@M_CustomersJson", M_CustomersJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
          

        }


    }
}
