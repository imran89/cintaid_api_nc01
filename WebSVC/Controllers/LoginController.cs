﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;
using System.Data;
using System;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public LoginController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task Get(string Username, string Password, string dt,string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            var cmd = new SqlCommand("Exec SP_Login @username,@password");
            cmd.Parameters.AddWithValue("@username", Username);
            cmd.Parameters.AddWithValue("@password", Password);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                await INS_Intruders(Username);

            }
        }

        ////SP_Get_ProfileID(@ID
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{ID}")]
        //public async Task GetID(string Username, string Password, string dt, string keys, int ID)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("Exec SP_Get_ProfileID @ID");
        //        cmd.Parameters.AddWithValue("@ID", ID);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    }
        //    else
        //    {
        //        await INS_Intruders(Username);

        //    }
        //}

        // Thomas Benny
        // 14 May 2018 - update 6 Juni 2018
        // Sepertinya Tidak digunakan lagi -- Update digunakan di OFFICEGO
        [HttpPost()]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            // if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            // {

            string M_UserJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("Exec SP_Get_Login_Json @M_UserJson");
            cmd.Parameters.AddWithValue("@M_UserJson", M_UserJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            //}
            //else
            //{
            //    await INS_Intruders(keys);

            //}
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Module}/{App}")]
        public string Get(string Username, string Password, string dt, string keys, string Module, string App)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                SqlConnection conn = null;
            SqlDataReader rdr = null;
            try
            {
                conn = new SqlConnection("Server=foodieGO.database.windows.net;Database=CintaID;User Id=thomas.benny;Password=Mcfurry.2011");
                conn.Open();
                    SqlCommand cmd = new SqlCommand("dbo.SP_GET_EMAIL", conn);
                    cmd.Parameters.AddWithValue("@Module", Module);
                    cmd.Parameters.AddWithValue("@App", App);
                    cmd.Parameters.AddWithValue("@Username", Username);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@keys", keys);
                    cmd.CommandType = CommandType.StoredProcedure;
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    CintaIDAPI.MessageServices.SendEmailAsync(Username, rdr["title"].ToString(), rdr["bodyHTML"].ToString(), rdr["bodyText"].ToString());

                }
                return @"{""status"":""email has been sent.""}";
            }
            catch (Exception ex)
            {

                return @"{""status"":""email not found.""}";
                //return ex.Message.ToString();
            }
            finally
            {

                if (conn != null)
                {
                    conn.Close();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            }
            else
            {
                 INS_Intruders(Username);
                return @"{""status"":""Intruders.""}";

            }
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Parameter}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Parameter)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                if (Parameter == "Checkusername")
                {
                    var cmd = new SqlCommand("exec SP_Get_Check_ID @username");
                    cmd.Parameters.AddWithValue("username", Username);
                    await SqlPipe.Stream(cmd, Response.Body, "{}");
                }
                else
                {

                }
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

    }
}
