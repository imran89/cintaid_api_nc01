﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class HelpController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public HelpController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{application}")]
        public async Task Get(string Username, string Password, string dt,string keys,int id,string application)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            var cmd = new SqlCommand("Exec SP_Help_ID @id,@application");
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@application", application);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                await INS_Intruders(Username);

            }
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{Search}/{application}")]
        public async Task Get(string Username, string Password, string dt, string keys, string id, string search, string application)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Help_Search @Search,@application");
                cmd.Parameters.AddWithValue("@Search", search);
                cmd.Parameters.AddWithValue("@application", application);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

    }
}
