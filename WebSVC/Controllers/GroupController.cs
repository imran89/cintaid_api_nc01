﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class GroupController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public GroupController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, int id)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_ItemGroupType_ID @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);
            }
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Division}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Division,string search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_ItemGroupType_Search @Division, @Search");
                cmd.Parameters.AddWithValue("Division", Division);
                cmd.Parameters.AddWithValue("Search", search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);
            }
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        [HttpPost("{Username}/{dt}/{keys}")]
        public async Task Post(string Username, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string M_GroupJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_M_Group_Json @M_GroupJson");
                cmd.Parameters.AddWithValue("@M_GroupJson", M_GroupJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{dt}/{keys}")]
        public async Task Delete(string Username, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string M_GroupJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Del_M_Group_Json @M_GroupJson");
                cmd.Parameters.AddWithValue("@M_GroupJson", M_GroupJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
