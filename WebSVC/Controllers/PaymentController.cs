﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class PaymentController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public PaymentController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{CustomerID}/")]
        public async Task Get(string Username, string Password, string dt, string keys, double CustomerID)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Payment_Search @CustomerID");
                cmd.Parameters.AddWithValue("CustomerID", CustomerID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);
            }

        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{MerchantID}/{PaymentID}/")]
        public async Task Get(string Username, string Password, string dt, string keys, double MerchantID, double PaymentID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Get_DetailPayment @MerchantID, @PaymentID");
                cmd.Parameters.AddWithValue("MerchantID", MerchantID);
                cmd.Parameters.AddWithValue("PaymentID", PaymentID);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }



        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_PaymentsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_Payments_Json @Tr_PaymentsJson");
                cmd.Parameters.AddWithValue("Tr_PaymentsJson", Tr_PaymentsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }



        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_PaymentJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec [Del_Tr_Payments_id] @Tr_PaymentJson");
                cmd.Parameters.AddWithValue("Tr_PaymentJson", Tr_PaymentJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }


        //// GET api/Todo/5 
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/")]
        //public async Task Get(string Username, string Password, string dt, string keys, double MERCHANT_ID, double id)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("exec SP_Get_Payment_ID @id");
        //        cmd.Parameters.AddWithValue("id", id);
        //        await SqlPipe.Stream(cmd, Response.Body, "[]");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }

        //}





    }
}