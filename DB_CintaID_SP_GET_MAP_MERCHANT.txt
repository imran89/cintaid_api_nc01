/****** Object:  StoredProcedure [dbo].[SP_Get_MapMerchant]    Script Date: 12/17/2018 5:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[SP_Get_MapMerchant] (@Selection as small_string,@Search as small_string,@Long as float,@Lad as float ,@Rad as int, @Tipe as Small_String)
AS
BEGIN
	IF (@Selection != 'dish' AND @Selection != 'store' AND @Selection != 'serv')
		BEGIN
		IF (@Search != '|')
		BEGIN
		SELECT * FROM (
		SELECT *,'true' cinta, 'true' fav, (CASE WHEN rate.ratings IS NULL THEN 0 ELSE rate.ratings END)as Rating, (CASE WHEN cinta.Ttl_Cinta IS NULL THEN 0 ELSE cinta.Ttl_Cinta END) Total_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) AS Total_Comments FROM M_Merchants 
				left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 1 and lks_isdelete = 'false'
					GROUP BY LKS_REFF_ID)Cinta on M_Merchants.mERCHANT_id = cinta.LKS_REFF_ID
					--left JOIN (	select isnull(CMT_Reff_ID,0)CMT_Reff_ID,isnull(count(CMT_Reff_ID),0)Ttl_Comments from TR_COMMENTS
					left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 1 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on  M_Merchants.mERCHANT_id = COMMENT.CMT_Reff_ID

					left JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) ratings, Rating_Reff_ID 
						from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 1  group by Rating_Reff_ID)rate on M_Merchants.mERCHANT_id = rate.Rating_Reff_ID

					WHERE Merchant_name like '%' + @Search + '%' AND Merchant_Image1 is not null
					AND dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= 100 AND Merchant_isActive = 'true' -- @Rad
					AND Merchant_App = @Tipe)x
		 FOR JSON AUTO,INCLUDE_NULL_VALUES
		END
		ELSE
		BEGIN
		
		SELECT * FROM (
		SELECT *,'true' cinta, 'true' fav, (CASE WHEN rate.ratings IS NULL THEN 0 ELSE rate.ratings END)as Rating, (CASE WHEN cinta.Ttl_Cinta IS NULL THEN 0 ELSE cinta.Ttl_Cinta END) Total_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) AS Total_Comments FROM M_Merchants 
				left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 1 and lks_isdelete = 'false'
					GROUP BY LKS_REFF_ID)Cinta on M_Merchants.mERCHANT_id = cinta.LKS_REFF_ID
					--left JOIN (	select isnull(CMT_Reff_ID,0)CMT_Reff_ID,isnull(count(CMT_Reff_ID),0)Ttl_Comments from TR_COMMENTS
					left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 1 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on  M_Merchants.mERCHANT_id = COMMENT.CMT_Reff_ID

					left JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) ratings, Rating_Reff_ID 
						from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 1  group by Rating_Reff_ID)rate on M_Merchants.mERCHANT_id = rate.Rating_Reff_ID

					
					WHERE LEN(Merchant_Image1) > 10 AND  dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= 100 AND Merchant_isActive = 'true'
					AND Merchant_App = @Tipe)x
					 --dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= @Rad
		 FOR JSON AUTO,INCLUDE_NULL_VALUES
		END
	END
	ELSE IF (@Selection = 'dish')
	BEGIN  
	IF (@Search != '|')
		BEGIN
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta], 
			'True' FlgFav 
			
		FROM M_Items_Merchants Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 2 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 2 and lks_isdelete = 'false')CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 2 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 2  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X

		WHERE (x.Merchant_Name like '%' + @Search + '%' or x.item_name like '%' + @Search + '%' or x.item_name_eng like '%' + @Search + '%' or item_desc like '%' + @Search + '%' or x.item_Desc_eng like '%' + @Search + '%')  
		AND dbo.GetDistanceBetween(@Lad, @Long, x.Merchant_latitude, x.Merchant_Longitude) <= 100 
		--@Rad 
		AND x.item_isales = 'true' and x.item_isactive = 'true' and x.item_isdelete ='false' AND x.Item_Image1 is not null
			 AND x.Merchant_App = @Tipe
		 FOR JSON AUTO, INCLUDE_NULL_VALUES
		END
		ELSE
		BEGIN
		
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) as Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta], 
			'True' FlgFav
		FROM M_Items_Merchants Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select lks_Reff_ID, count(LKS_Reff_ID) Ttl_Cinta from TR_LIKES
					where lks_Type = 2 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 2 and LKS_InsertBy = 8)CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 2 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 2  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X
		WHERE dbo.GetDistanceBetween(@Lad, @Long, x.Merchant_latitude, x.Merchant_Longitude) <= 100
		AND x.item_isales = 'true' and x.item_isactive = 'true' and x.item_isdelete ='false' AND x.Item_Image1 is not null 	 
		AND x.Merchant_App = @Tipe
		FOR JSON AUTO, INCLUDE_NULL_VALUES
		END
	END
	ELSE IF (@Selection = 'serv')
	BEGIN
	IF (@Search != '|')
		BEGIN
		
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta],
			'True' FlgFav
		FROM M_Items_Merchants_Serv Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 23 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 23 and lks_isdelete = 'false')CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 23 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 23  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X
		WHERE (x.item_name like '%' + @Search + '%' or x.item_name_eng like '%' + @Search + '%' or x.item_desc like '%' + @Search + '%' or item_Desc_eng like '%' + @Search + '%')  
		AND dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= @Rad 
		--@Rad 
		AND x.item_isales = 'true' and x.item_isactive = 'true' and x.item_isdelete ='false' AND x.Item_Image1 is not null
			 AND x.Merchant_App = @Tipe
		 FOR JSON AUTO, INCLUDE_NULL_VALUES
		END
		ELSE
		BEGIN
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta],
			'True' FlgFav
		FROM M_Items_Merchants_Serv Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 23 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 23 and lks_isdelete = 'false')CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 23 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 23  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X
		WHERE dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= 100
		--@Rad 
		AND x.item_isales = 'true' and x.item_isactive = 'true' and x.item_isdelete ='false' AND x.Item_Image1 is not null 	 
		AND x.Merchant_App = @Tipe

		 FOR JSON AUTO,INCLUDE_NULL_VALUES
		END
	END
	ELSE IF (@Selection = 'store')
	BEGIN
	IF (@Search != '|')
		BEGIN
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta], 
			'True' FlgFav
		FROM M_Items_Merchants_Store Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 22 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 22 and lks_isdelete = 'false')CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 22 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 22  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X
		WHERE (Merchant_Name like '%' + @Search + '%' or item_name like '%' + @Search + '%' or item_name_eng like '%' + @Search + '%' or item_desc like '%' + @Search + '%' or item_Desc_eng like '%' + @Search + '%')  
			and dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= 100  and
			item_isales = 'true' and item_isactive = 'true' and item_isdelete ='false' --AND Item_Image1 is not null
				 AND Merchant_App = @Tipe
		 FOR JSON AUTO, INCLUDE_NULL_VALUES
		END
		ELSE
		BEGIN
		SELECT TOP 50 * FROM (
		SELECT Merchant.*,
			t.TYPE_NAME, IG.ITM_Group_Name,
			Itm.Item_ID, Itm.Item_Name, Itm.Item_Name_ENG, Itm.Item_Desc_ENG, Itm.Item_Desc, Itm.Item_Image1, Itm.Item_Image2, Itm.Item_Price, Itm.Item_LastPrice, 
			Itm.Item_GroupType_ID, Itm.item_ISales, Itm.Item_IsPurchase, Itm.Item_isActive, Itm.item_isDelete,
			(CASE WHEN Cinta.Ttl_Cinta IS NULL THEN 0 ELSE Cinta.Ttl_Cinta END) Ttl_Cinta, 
			(CASE WHEN COMMENT.Ttl_Comments IS NULL THEN 0 ELSE COMMENT.Ttl_Comments END) Ttl_Comments,
			(CASE WHEN rate.rating IS NULL THEN 0 ELSE rate.rating END) Rating, 
			(CASE WHEN CintaFlg.LKS_Flag IS NULL THEN 0 ELSE CintaFlg.LKS_Flag END) as [FlgCinta], 
			'True' FlgFav
		FROM M_Items_Merchants_Store Itm
		INNER JOIN M_Merchants Merchant  WITH(NOLOCK) ON Itm.Item_Merchant_ID = Merchant.Merchant_ID
		left JOIN (	select isnull(LKS_Reff_ID,0)LKS_Reff_ID,isnull(count(lks_Reff_ID),0)Ttl_Cinta from TR_LIKES
					where lks_Type = 22 and LKS_Flag = 1 
					GROUP BY LKS_REFF_ID)Cinta on Itm.item_ID = cinta.LKS_REFF_ID
		left JOIN (	select LKS_REFF_ID, LKS_Flag from TR_LIKES
					where lks_Type = 22 and lks_isdelete = 'false')CintaFlg on Itm.item_ID = CintaFlg.LKS_REFF_ID
		left JOIN (	select CMT_Reff_ID, COUNT(CMT_Reff_ID)Ttl_Comments from TR_COMMENTS
					where CMT_Type = 22 and CMT_isdelete = 'false'
					GROUP BY CMT_REFF_ID)COMMENT on Itm.item_ID = COMMENT.CMT_Reff_ID
		INNER JOIN M_Items_Group AS IG ON Itm.Item_GroupType_ID = IG.ITM_Group_ID
		INNER JOIN M_Type AS T ON IG.ITM_Group_ItemType = T.TYPE_ID
		LEFT JOIN (select CONVERT(DECIMAL(18,2), (sum(Rating_Value) / isnull(count(Rating_Reff_ID),0))) rating, Rating_Reff_ID 
			from Tr_Rating where Rating_IsDelete = 'false' and Rating_Type_ID = 22  
			group by Rating_Reff_ID)rate on Itm.item_ID = rate.Rating_Reff_ID) X
		WHERE dbo.GetDistanceBetween(@Lad, @Long, Merchant_latitude, Merchant_Longitude) <= 100 and
		item_isales = 'true' and item_isactive = 'true' and item_isdelete ='false' --AND Item_Image1 is not null 	 
		AND Merchant_App = @Tipe

		FOR JSON AUTO,INCLUDE_NULL_VALUES
		END
	END
END



